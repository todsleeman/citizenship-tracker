import axios from 'axios'

export default () => {
    return axios.create({
        baseURL: 'https://citizenship-server.herokuapp.com/'
    })
}